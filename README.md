# Chunk Array

Chunk Array is pure python package that allows "chunking" of numpy ndarrays for distributed processing.  It appends list of dictionaries to the ndarray object and provides some convenience function to access them.

#### Dictionary Keys
There is one dictionary for each axis in the ndarray and the list index corresponds directly to the same axis in the ndarray.

Key | Description
:----:|-------
**full_start** | Inclusive integer defining global array start pixel
**full_stop** |  Exclusive integer defining global array stop pixel
**full_shape** |  Integer defining global array
**chunk_shape** | Integer defining the number of chunks the global array is split
**chunk_ndx** | Integer defining this array's chunk index within the distributed layout
**padding** | tuple defining the number of pixels appened to the beginning and end of the array

#### Usage
Chunk Arrays can be created in the following way:
```python
import numpy as np
import chunkarray as ca

array = np.random.randn(100, 100)

# Method 1
chunk_array = ca.ChunkArray(array)

# Method 2
chunk_array = array.view(ca.ChunkArray)
```

#### Chunking
Partitioning an ndarray into small "sub-arrays" is beneficial for distributing a large array, for use with either Dask's Bags or Spark's RDDs.  This can be accomplished using the *chunker_nd* and *extract_chunks_from_array* functions.

```python
array = np.random.randn(100, 200)

num_chunks = (4, 6) # 4 Chunks for axis 0, 6 chunks for axis 1
padding = (2, 1) # pad axis 0 by 2 pixels on each side, axis 1 by 1
chunk_dicts = ca.chunker_nd(array.shape, num_chunks, padding)

print(chunk_dicts[4])
"""
{'chunk_ndx': 0,
 'chunk_shape': 4,
 'full_shape': 100,
 'full_start': 0,
 'full_stop': 27,
 'padding': (0, 2)}
"""

# To create the chunk arrays
chunks = ca.extract_chunks_from_array(chunk_dicts, array)
```

#### Combining Chunks
Taking a list of chunk arrays and combining them together can be accomplished by using the *full_array_from_chunks* or *bounding_array_from_chunks* functions.  The former always creates an array the size of the global array, the latter computes the minimum rectangular array containing the supplied chunk arrays.

```python
array = np.random.randn(90, 90)

num_chunks = (3, 3)
padding = (0, 0)
chunk_dicts = ca.chunker_nd(array.shape, num_chunks, padding)
chunks = ca.extract_chunks_from_array(chunk_dicts, array)

# Create a full array with only chunk[0] inserted
full_array = ca.full_array_from_chunks([chunks[0]])
print(full_array.shape)
#>> (90, 90)

# Create a bounding box array
partial_array = ca.bounding_array_from_chunks([chunks[0], chunks[4]])
print(partial_array.shape)
#>> (60, 60)
```

#### Rechunking
There are times when changing the chunk paradigm is desired, such as being contiguous along different axis.  This can be accomplished with the **rechunk** function.
```python
array = np.random.randn(100, 200)

# contiguous in the row (horizontal) direction
num_chunks = (10, 1)
padding = (0, 0)
chunk_dicts = ca.chunker_nd(array.shape, num_chunks, padding)
chunks = ca.extract_chunks_from_array(chunk_dicts, array)
print(chunks[0].shape)
#>> (10, 200)

# convert to contigous in the column (vertical) direction
num_chunks = (1, 10)
new_chunks = ca.rechunk(chunks, num_chunks, padding)
print(new_chunks[0].shape)
#>> (100, 20)
```

#### Rechunking with Spark
Rechunk has an rdd version that can be used with Apache Spark, **rechunk_rdd**

```python
import numpy as np
import chunkarray as ca

the_array = np.random.randn(100, 200)
the_chunks = ca.chunker_nd(the_array.shape, num_chunks=(1, 10))
the_list = ca.extract_chunks_from_array(the_chunks, the_array)
rdd = sc.parallelize(the_list)

new_chunk_layout_rdd = ca.rechunk_rdd(rdd, num_chunks=(10, 1))

new_list = new_chunk_layout_rdd.collect()
print(new_list[0].get_chunk_dict())
"""
({'chunk_ndx': 0,
  'chunk_shape': 10,
  'full_shape': 100,
  'full_start': 0,
  'full_stop': 10,
  'padding': (0, 0)},
 {'chunk_ndx': 0,
  'chunk_shape': 1,
  'full_shape': 200,
  'full_start': 0,
  'full_stop': 200,
  'padding': (0, 0)})
"""
```
