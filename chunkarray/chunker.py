from itertools import product, starmap
from chunkarray.chunkarray import ChunkArray
import numpy as np


def chunker_1d(shape, num_chunks=2, padding=0):
    """Generates chunk dictionaries to split an array.

    Args:
        shape:  Integer representing length of the array
        num_chunks: Number of chunks to split the data into
        padding:  number of pad pixels to keep

    Returns:
        list of chunk dictionaries
    """
    no_pad_ndcs = np.linspace(0, shape, num_chunks + 1).astype('int')
    start_ndcs = (no_pad_ndcs - padding).clip(0, shape)
    stop_ndcs = (no_pad_ndcs + padding).clip(0, shape)

    def populate_chunk_dict(chunk_ndx):
        pad_before = no_pad_ndcs[chunk_ndx] - start_ndcs[chunk_ndx]
        pad_after = stop_ndcs[chunk_ndx + 1] - no_pad_ndcs[chunk_ndx + 1]
        return {'full_start': start_ndcs[chunk_ndx],
                'full_stop': stop_ndcs[chunk_ndx + 1],
                'full_shape': shape, 'chunk_ndx': chunk_ndx,
                'chunk_shape': num_chunks,
                'padding': (pad_before, pad_after)}

    return list(map(populate_chunk_dict, range(num_chunks)))


def chunker_nd(shape, num_chunks=(2, 2), padding=(0, 0)):
    """Generates chunk dictionaries for an n-dimensional array.

    Args:
        shape: Tuple of Integers of the shape of the array
        num_chunks: Number of chunks in each direction to split the data into
        padding: Padding in each direction of the valid data

    Returns:
        lists of chunk dictionaries
    """
    return list(product(*starmap(chunker_1d, zip(shape, num_chunks, padding))))


def extract_chunks_from_array(chunk_list, array):
    """Extracts chunks from an array.

    Args:
        chunk_list:  list of chunk dictionaries
        array:  array to pull data from

    Returns:
        a list of ChunkArrays
    """
    def convert_to_slice(chunk_dicts):
        return [slice(chunk_dict['full_start'], chunk_dict['full_stop'])
                for chunk_dict in chunk_dicts]

    return [ChunkArray(array[slices], chunk_list[ndx]) for ndx, slices in
            enumerate(map(convert_to_slice, chunk_list))]
