from copy import deepcopy

import numpy as np


def default_chunk_dict(shape=0):
    """Creates a default chunk dictionary for distributed arrays.

    Args:
        shape:  full array shape

    Returns:
        chunk dictionary
    """
    return {'full_start': 0, 'full_stop': shape, 'full_shape': shape,
            'chunk_ndx': 0, 'chunk_shape': 1, "padding": (0, 0)}


class ChunkArray(np.ndarray):
    """Numpy ndarray for distributing data across multiple nodes.

    ChunkArray keeps track of the arrays position in the full array.
    """

    def __new__(cls, input_array, chunk_dict=None):
        obj = np.asarray(input_array).view(cls)
        obj.__chunk_dict__ = deepcopy(chunk_dict)
        if chunk_dict is None:
            obj.__chunk_dict__ = tuple([default_chunk_dict(shape) for
                                        shape in obj.shape])
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        _chunk_dict = tuple([default_chunk_dict(shape) for shape in obj.shape])
        self.__chunk_dict__ = getattr(obj, '__chunk_dict__', _chunk_dict)

    def __reduce__(self):
        """Used for pickling purpose."""
        pickled_state = super(ChunkArray, self).__reduce__()
        new_state = pickled_state[2] + (self.__chunk_dict__,)
        return (pickled_state[0], pickled_state[1], new_state)

    def __setstate__(self, state):
        """Restores from pickle."""
        self.__chunk_dict__= state[-1]
        super(ChunkArray, self).__setstate__(state[0:-1])

    def set_chunk_dict(self, new_dict):
        self.__chunk_dict__ = deepcopy(new_dict)

    def get_chunk_dict(self):
        return deepcopy(self.__chunk_dict__)

    def valid_array(self):
        """Returns an array with no padding."""
        padding = deepcopy(self.padding)
        slices = [slice(padding[ndx, 0], self.shape[ndx] - padding[ndx, 1])
                  for ndx in range(self.ndim)]

        # Trim the data
        valid_array = self[slices]

        # Update the dictionaries
        chunk_dicts = self.get_chunk_dict()
        for chunk_dict in chunk_dicts:
            chunk_dict['full_start'] = chunk_dict['full_start'] + chunk_dict['padding'][0]
            chunk_dict['full_stop'] = chunk_dict['full_stop'] - chunk_dict['padding'][1]
            chunk_dict['padding'] = (0, 0)

        valid_array.set_chunk_dict(chunk_dicts)
        return valid_array

    @property
    def full_shape(self):
        return np.array([chunk_dict['full_shape'] for chunk_dict in self.__chunk_dict__])

    @property
    def full_start(self):
        return np.array([chunk_dict['full_start'] for chunk_dict in self.__chunk_dict__])

    @property
    def full_stop(self):
        return np.array([chunk_dict['full_stop'] for chunk_dict in self.__chunk_dict__])

    @property
    def chunk_ndx(self):
        return np.array([chunk_dict['chunk_ndx'] for chunk_dict in self.__chunk_dict__])

    @property
    def chunk_shape(self):
        return np.array([chunk_dict['chunk_shape'] for chunk_dict in self.__chunk_dict__])

    @property
    def padding(self):
        """padding[:, 0] is pad before, padding[:, 1] is pad after."""
        return np.array([chunk_dict['padding'] for chunk_dict in self.__chunk_dict__])
