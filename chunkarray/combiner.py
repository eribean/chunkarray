from chunkarray.chunkarray import ChunkArray
import numpy as np


def _insert(the_array_list, output_array):
    """Inserts the array list into the output array."""
    for array in the_array_list:
        padding = array.padding
        full_start = array.full_start - output_array.full_start
        full_stop = array.full_stop - output_array.full_start

        global_slice = [slice(full_start[ndx] + padding[ndx, 0],
                              full_stop[ndx] - padding[ndx, 1])
                        for ndx in range(array.ndim)]

        output_array[global_slice] = array.valid_array()

    return output_array


def full_array_from_chunks(the_array_list):
    """Creates a full array from the chunks.

    Args:
        the_array_list:  a list of arrays to combine into large image

    Returns:
        ChunkArray of the total full shape
    """
    output_array = np.zeros(the_array_list[0].full_shape).view(ChunkArray)

    return _insert(the_array_list, output_array)


def bounding_array_from_chunks(the_array_list):
    """Combines chunks in smallest possible array.

    Args:
        the_array_list:  a list of arrays to combine together

    Returns:
        ChunckArray of the bounding box of the arrays in list
    """
    global_stop = np.zeros((the_array_list[0].ndim,), dtype=int)
    global_start = np.full((the_array_list[0].ndim,), 2**31)

    # TODO:  Change the order
    for ndx in range(the_array_list[0].ndim):
        for array in the_array_list:
            padding = array.padding
            full_start = array.full_start
            full_stop = array.full_stop

            global_start[ndx] = min(full_start[ndx] + padding[ndx, 0], global_start[ndx])
            global_stop[ndx] =  max(full_stop[ndx] - padding[ndx, 1], global_stop[ndx])

    output_array = np.zeros(global_stop - global_start).view(ChunkArray)

    # fix the chunk dictionaries
    chunk_dict = output_array.get_chunk_dict()
    for ndx, the_dict in enumerate(chunk_dict):
        the_dict['full_shape'] = the_array_list[0].full_shape[ndx]
        the_dict['full_start'] = global_start[ndx]
        the_dict['full_stop'] = global_stop[ndx]
        the_dict['padding'] = (0, 0)

    output_array.set_chunk_dict(chunk_dict)

    return _insert(the_array_list, output_array)
