from itertools import groupby

import numpy as np

from chunkarray import ChunkArray, chunker_nd


def chunk_intersection(old_chunk, new_layout):
    """Partitions the old chunk array into subsets of the new layout.

    Args:
        old_chunk:  One chunk array in the old layout
        new_layout: list of chunk dictionaries defining the new layout

    Returns:
        list of subset chunk arrays with dictionaries populated relative
        to the new layout corner
    """
    # only the valid region will contribute in the old chunk
    old_start = old_chunk.full_start
    old_stop = old_chunk.full_stop
    valid_start = old_start + old_chunk.padding[:, 0]
    valid_stop = old_stop - old_chunk.padding[:, 1]
    ndim = range(old_start.size)

    intersection_dicts = list()
    for new_dict in new_layout:

        # Check intersection
        bound1 = (valid_stop[ndx] <= new_dict[ndx]['full_start'] for ndx in ndim)
        bound2 = (new_dict[ndx]['full_stop'] <= valid_start[ndx] for ndx in ndim)
        if any(bound1) or any(bound2):
          continue

        int_start = [max(valid_start[ndx], new_dict[ndx]['full_start']) for ndx in ndim]
        int_stop =  [min(valid_stop[ndx], new_dict[ndx]['full_stop']) for ndx in ndim]

        # Extract the subset, set the insertion point relative to the new layout
        slices = [slice(start,stop) for start, stop in
                  zip(int_start-old_start, int_stop-old_start)]
        subset = old_chunk[slices]

        the_dict = subset.get_chunk_dict()
        for ndx in ndim:
            the_dict[ndx]['full_start'] = int_start[ndx] - new_dict[ndx]['full_start']
            the_dict[ndx]['full_stop'] = int_stop[ndx] - new_dict[ndx]['full_start']
            the_dict[ndx]['full_shape'] = new_dict[ndx]['full_stop'] - new_dict[ndx]['full_start']
        subset.set_chunk_dict(the_dict)

        # Linear Index to use as key for grouping
        linear_ndx = new_dict[0]['chunk_ndx']
        for ndx in range(1, old_start.size):
            linear_ndx = (new_dict[ndx]['chunk_shape'] * linear_ndx +
                          new_dict[ndx]['chunk_ndx'])

        intersection_dicts.append((linear_ndx, subset))

    return intersection_dicts


def assemble_chunks(chunk_list, chunk_dict):
    """Takes a list of chunk arrays and inserts them into a larger array.

    Args:
        chunk_list:  list of subset chunk arrays
        chunk_dict:  chunk dictionary that defines how to assemble the list

    Returns:
        Assembled ChunkArray
    """
    big_chunk = np.zeros(chunk_list[0].full_shape).view(ChunkArray)
    big_chunk.set_chunk_dict(chunk_dict)

    for chunk in chunk_list:
        slices = [slice(start, stop) for start, stop in
                  zip(chunk.full_start, chunk.full_stop)]
        big_chunk[slices] = chunk

    return big_chunk


def rechunk(chunk_list, num_chunks=(2, 2), padding=(0, 0)):
    """Changes the layout of a previous n-dimensional chunk.

    Args:
        chunk_list: list of ChunkArrays in current chunk layout
        num_chunks: Number of chunks in each direction to split the data into
        padding: Padding in each direction of the valid data

    Returns:
        new chunk list in the desired chunk layout
    """
    new_chunk_dicts = chunker_nd(chunk_list[0].full_shape, num_chunks, padding)

    intermediate_chunks = list()
    for old_chunk in chunk_list:
        intermediate_chunks += chunk_intersection(old_chunk, new_chunk_dicts)

    new_chunk_list = list()
    def key(x): return x[0]
    for key, value in groupby(sorted(intermediate_chunks, key=key), key=key):
        chunk_list = list(map(lambda x: x[1],  value))
        new_chunk_list.append(assemble_chunks(chunk_list, new_chunk_dicts[key]))

    return new_chunk_list


def rechunk_rdd(rdd, num_chunks=(2,2), padding=(0, 0)):
    """Changes the layout of a previous n-dimensional chunk.

    Args:
        rdd: rdd of ChunkArrays in current layout
        num_chunks: Number of chunks in each direction to split the data into
        padding: Padding in each direction of the valid data

    Returns:
        new rdd in the desired chunk layout
    """
    full_shape = rdd.map(lambda x: x.full_shape).first()
    new_chunk_dicts = chunker_nd(full_shape, num_chunks, padding)

    def intermediate_chunks(the_array):
        return chunk_intersection(the_array, new_chunk_dicts)

    def final_chunks(the_array):
        ndx = the_array[0]
        return assemble_chunks(list(the_array[1]), new_chunk_dicts[ndx])

    new_rdd = rdd.flatMap(intermediate_chunks)
    new_rdd = new_rdd.groupByKey()
    return new_rdd.map(final_chunks)
