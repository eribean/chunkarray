import unittest

import numpy as np

from chunkarray import chunker_nd, rechunk, rechunk_rdd
from chunkarray import ChunkArray, extract_chunks_from_array

def chunk_lists_equal(list1, list2):
    """Compares to chunklists for equality."""

    for data in list1:
        chunk_ndx = data.chunk_ndx
        for other_data in list2:
            if np.all(chunk_ndx == other_data.chunk_ndx):
                np.testing.assert_array_equal(data, other_data)
                assert(data.__chunk_dict__[0] == other_data.__chunk_dict__[0])
                assert(data.__chunk_dict__[1] == other_data.__chunk_dict__[1])


class TestRechunk(unittest.TestCase):
    """Class to test the rechunking logic."""
    def extract_chunks(self, shape, num_chunks, padding):
        """Helper function retuning chunk_array list."""
        chunk_dicts = chunker_nd(shape, num_chunks, padding)
        return extract_chunks_from_array(chunk_dicts, self.array)

    def setUp(self):
        self.shape = (243, 653)
        self.num_chunks = (7, 13)
        self.padding = (3, 5)
        self.array = np.zeros(self.shape).view(ChunkArray)
        self.chunk_list = self.extract_chunks(self.shape, self.num_chunks,
                                              self.padding)

    def test_unitary_rechunk(self):
        """Testing the same parameters for rechunking are valid."""
        new_chunk_list = rechunk(self.chunk_list, self.num_chunks,
                                    self.padding)

        self.assertEqual(len(new_chunk_list), len(self.chunk_list))
        chunk_lists_equal(new_chunk_list, self.chunk_list)

    def test_rechunk(self):
        """Testing a random rechunks."""
        chunks = [(23, 11), (13, 9), (8, 3), (1, 15), (15, 1)]
        paddings = [(5, 2), (0, 0), (3, 4), (7, 0), (1, 1)]
        for num_chunks, padding in zip(chunks, paddings):
            new_chunk_list = rechunk(self.chunk_list, num_chunks, padding)

            truth_chunk_list = self.extract_chunks(self.shape, num_chunks, padding)

            self.assertEqual(len(new_chunk_list), len(truth_chunk_list))
            chunk_lists_equal(new_chunk_list, truth_chunk_list)

class TestRechunk3D(unittest.TestCase):
    """Class to test the rechunking logic on 3d array."""
    def extract_chunks(self, shape, num_chunks, padding):
        """Helper function retuning chunk_array list."""
        chunk_dicts = chunker_nd(shape, num_chunks, padding)
        return extract_chunks_from_array(chunk_dicts, self.array)

    def setUp(self):
        self.shape = (101, 87, 80)
        self.num_chunks = (7, 5, 3)
        self.padding = (3, 5, 4)
        self.array = np.zeros(self.shape).view(ChunkArray)
        self.chunk_list = self.extract_chunks(self.shape, self.num_chunks,
                                              self.padding)

    def test_unitary_rechunk(self):
        """Testing the same parameters for rechunking are valid."""
        new_chunk_list = rechunk(self.chunk_list, self.num_chunks,
                                    self.padding)

        self.assertEqual(len(new_chunk_list), len(self.chunk_list))
        chunk_lists_equal(new_chunk_list, self.chunk_list)

    def test_rechunk(self):
        """Testing a random rechunks."""
        chunks = [(3, 1, 11), (7, 7, 7)]
        paddings = [(5, 2, 1), (0, 0, 0)]
        for num_chunks, padding in zip(chunks, paddings):
            new_chunk_list = rechunk(self.chunk_list, num_chunks, padding)

            truth_chunk_list = self.extract_chunks(self.shape, num_chunks, padding)

            self.assertEqual(len(new_chunk_list), len(truth_chunk_list))
            chunk_lists_equal(new_chunk_list, truth_chunk_list)

try:
    import pysparkling

    class TestRechunkRDD(unittest.TestCase):
        """Testing rechunk on an rdd."""

        def setUp(self):
            self.sc = pysparkling.Context()

        def test_rechunk_rdd(self):
            """Testing the using rdd's for rechunking data."""
            the_array = np.random.randn(100, 200)
            the_chunks = chunker_nd(the_array.shape, num_chunks=(1, 10), padding=(3, 4))
            the_list = extract_chunks_from_array(the_chunks, the_array)
            rdd = self.sc.parallelize(the_list)

            num_chunks = (10, 1)
            padding = (4, 3)
            new_rdd = rechunk_rdd(rdd, num_chunks, padding)
            new_list = new_rdd.collect()

            truth_list = rechunk(the_list, num_chunks, padding)
            chunk_lists_equal(new_list, truth_list)

except ImportError:
    print("No pysparkling import found, not testing rechunk_rdd functions.")


if __name__ == '__main__':
    unittest.main()
