import unittest
import pickle
from copy import deepcopy

import numpy as np

from chunkarray import ChunkArray, default_chunk_dict


class TestChunkArray(unittest.TestCase):
    def test_default_chunk_dict(self):
        """Testing the creation of a default chunk dictionaries."""
        zero_dict = default_chunk_dict()
        self.assertEqual(zero_dict['full_shape'], 0)
        self.assertEqual(zero_dict['full_start'], 0)
        self.assertEqual(zero_dict['full_stop'], 0)
        self.assertEqual(zero_dict['chunk_ndx'], 0)
        self.assertEqual(zero_dict['chunk_shape'], 1)
        self.assertTupleEqual(zero_dict['padding'], (0, 0))

    def test_created_chunk_dict(self):
        """Testing the creation of a chunk dictionaries."""
        zero_dict = default_chunk_dict(17)
        self.assertEqual(zero_dict['full_shape'], 17)
        self.assertEqual(zero_dict['full_start'], 0)
        self.assertEqual(zero_dict['full_stop'], 17)
        self.assertEqual(zero_dict['chunk_ndx'], 0)
        self.assertEqual(zero_dict['chunk_shape'], 1)
        self.assertTupleEqual(zero_dict['padding'], (0, 0))

    def test_chunk_array_creation(self):
        """Testing the two ways to create the chunk array."""
        the_array = np.random.randn(85, 43)

        chunk_array1 = ChunkArray(the_array)
        self.assertIs(type(chunk_array1), ChunkArray)
        self.assertTrue(hasattr(chunk_array1, "__chunk_dict__"))
        self.assertIs(type(the_array), np.ndarray)

        chunk_array2 = the_array.view(ChunkArray)
        self.assertIs(type(chunk_array2), ChunkArray)
        self.assertTrue(hasattr(chunk_array2, "__chunk_dict__"))

    def test_chunk_properties(self):
        """Testing the retrieval of chunk properties."""
        the_array = np.random.rand(112,).view(ChunkArray)

        self.assertEqual(the_array.full_shape[0], 112)
        self.assertEqual(the_array.full_start[0], 0)
        self.assertEqual(the_array.full_stop[0], 112)
        self.assertEqual(the_array.chunk_ndx[0], 0)
        self.assertEqual(the_array.chunk_shape[0], 1)
        self.assertEqual(the_array.padding[0][0], 0)
        self.assertEqual(the_array.padding[0][1], 0)

    def test_chunk_properties_2d(self):
        """Testing the retrieval of 2d chunk dictionaries."""
        the_array = np.random.rand(321, 42).view(ChunkArray)

        np.testing.assert_array_equal(the_array.full_shape, [321, 42])
        np.testing.assert_array_equal(the_array.full_start, [0, 0])
        np.testing.assert_array_equal(the_array.full_stop, [321, 42])
        np.testing.assert_array_equal(the_array.chunk_ndx, [0, 0])
        np.testing.assert_array_equal(the_array.chunk_shape, [1, 1])
        np.testing.assert_array_equal(the_array.padding[0], [0, 0])
        np.testing.assert_array_equal(the_array.padding[1], [0, 0])

    def test_chunk_dictionary_one(self):
        """Testing setting dictionary at view time."""
        the_dict = default_chunk_dict(47)
        the_dict['padding'] = (3, 4)
        the_dict['full_start'] = 3
        the_dict['full_stop'] = 43
        the_dict['chunk_ndx'] = 10
        the_dict['chunk_shape'] = 21
        the_array = ChunkArray(np.random.rand(47,), deepcopy(the_dict))

        self.assertDictEqual(the_dict, the_array.__chunk_dict__)

    def test_chunk_dictionary_two(self):
        """Testing setting dictionary at view time."""
        the_dict = default_chunk_dict(47)
        the_dict['padding'] = (3, 4)
        the_dict['full_start'] = 3
        the_dict['full_stop'] = 43
        the_dict['chunk_ndx'] = 10
        the_dict['chunk_shape'] = 21
        the_array = ChunkArray(np.random.rand(47,))
        the_array.set_chunk_dict(the_dict)
        the_new_dict = the_array.get_chunk_dict()

        self.assertDictEqual(the_dict, the_new_dict)

        # Make sure it is copied
        the_dict['full_start'] = 32
        self.assertNotEqual(the_array.__chunk_dict__['full_start'],
                            the_dict['full_start'])

    def test_extract_valid_array(self):
        """Testing the extraction of the valid array."""

        the_array = np.random.randn(74, 105)

        the_dict1 = default_chunk_dict(47)
        the_dict1['padding'] = (3, 4)
        the_dict1['full_start'] = 0
        the_dict1['full_stop'] = 74
        the_dict2 = default_chunk_dict(47)
        the_dict2['padding'] = (5, 7)
        the_dict2['full_start'] = 0
        the_dict2['full_stop'] = 105

        ca_array = ChunkArray(deepcopy(the_array), [the_dict1, the_dict2])

        valid_array = ca_array.valid_array()

        np.testing.assert_array_equal(valid_array, the_array[3:70, 5:98])

        np.testing.assert_array_equal(valid_array.full_start, [3, 5])
        np.testing.assert_array_equal(valid_array.full_stop, [70, 98])
        np.testing.assert_array_equal(valid_array.padding, np.zeros((2, 2)))

    def test_pickle_chunk_array(self):
        """Testing that pickle / unpickle restores all values."""
        the_dict = default_chunk_dict(47)
        the_dict['padding'] = (3, 4)
        the_dict['full_start'] = 3
        the_dict['full_stop'] = 43
        the_dict['chunk_ndx'] = 10
        the_dict['chunk_shape'] = 21
        the_array = ChunkArray(np.random.rand(47,))
        the_array.set_chunk_dict(the_dict)

        new_array = pickle.loads(pickle.dumps(the_array, protocol=2))
        the_new_dict = new_array.get_chunk_dict()
        self.assertDictEqual(the_dict, the_new_dict)
        print("H")


if __name__ == '__main__':
    unittest.main()
