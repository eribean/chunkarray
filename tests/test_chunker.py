import unittest
import numpy as np

from chunkarray import chunker_1d, chunker_nd, extract_chunks_from_array


class TestChunker(unittest.TestCase):
    """Test class for chunking functions."""

    def test_chunker_1d(self):
        """Testing 1-dimensional chunking."""
        the_dicts = chunker_1d(99, num_chunks=3, padding=3)

        starts = (0, 30, 63)
        stops = (36, 69, 99)
        paddings = ((0, 3), (3, 3), (3, 0))

        for ndx, the_dict in enumerate(the_dicts):
            self.assertEqual(the_dict['full_start'], starts[ndx])
            self.assertEqual(the_dict['full_stop'], stops[ndx])
            self.assertEqual(the_dict['chunk_ndx'], ndx)
            self.assertEqual(the_dict['chunk_shape'], 3)
            self.assertEqual(the_dict['full_shape'], 99)
            np.testing.assert_array_equal(the_dict['padding'], paddings[ndx])

    def test_chunker_nd(self):
        """Testing 2-dimensional_chunking."""
        the_dicts = chunker_nd((66, 99), num_chunks=(3, 3), padding=(2, 7))
        row_dicts, col_dicts = zip(*the_dicts)

        starts = (0, 20, 42)
        stops = (24, 46, 66)
        paddings = ((0, 2), (2, 2), (2, 0))

        for ndx, the_dict in enumerate(row_dicts):
            ndx2 = int(ndx / 3)
            self.assertEqual(the_dict['full_start'], starts[ndx2])
            self.assertEqual(the_dict['full_stop'], stops[ndx2])
            self.assertEqual(the_dict['chunk_ndx'], ndx2)
            self.assertEqual(the_dict['chunk_shape'], 3)
            self.assertEqual(the_dict['full_shape'], 66)
            np.testing.assert_array_equal(the_dict['padding'], paddings[ndx2])

        starts = (0, 26, 59)
        stops = (40, 73, 99)
        paddings = ((0, 7), (7, 7), (7, 0))

        for ndx, the_dict in enumerate(col_dicts):
            ndx2 = ndx % 3
            self.assertEqual(the_dict['full_start'], starts[ndx2])
            self.assertEqual(the_dict['full_stop'], stops[ndx2])
            self.assertEqual(the_dict['chunk_ndx'], ndx2)
            self.assertEqual(the_dict['chunk_shape'], 3)
            self.assertEqual(the_dict['full_shape'], 99)
            np.testing.assert_array_equal(the_dict['padding'], paddings[ndx2])

    def test_chunk_extract_2d(self):
        """Testing the extraction of a chunk from a 2d-array."""
        the_dicts = chunker_nd((313, 78), num_chunks=(4, 3), padding=(2, 7))
        the_array = np.random.randn(313, 78)

        the_chunks = extract_chunks_from_array(the_dicts, the_array)

        for the_chunk in the_chunks:
            the_dict = the_chunk.get_chunk_dict()
            start_row = the_dict[0]['full_start']
            stop_row = the_dict[0]['full_stop']
            start_col = the_dict[1]['full_start']
            stop_col = the_dict[1]['full_stop']

            np.testing.assert_array_equal(the_chunk, the_array[start_row:stop_row,
                                                               start_col:stop_col])

if __name__ == '__main__':
    unittest.main()
