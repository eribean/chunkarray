import unittest

import numpy as np

from chunkarray import chunker_nd, extract_chunks_from_array
from chunkarray import full_array_from_chunks
from chunkarray import bounding_array_from_chunks


class TestCombiner(unittest.TestCase):
    """Test class to put back together chunks."""

    def test_full_array_from_chunks(self):
        """Testing putting chunks back together into a full array."""
        the_vector = np.random.randn(47, 83)
        the_dicts = chunker_nd(the_vector.shape, (4, 2), (0, 3))

        the_chunks = extract_chunks_from_array(the_dicts, the_vector)

        reassembled_vector = full_array_from_chunks(the_chunks)

        np.testing.assert_array_equal(the_vector, reassembled_vector)

    def test_bounding_arrary_from_chunks(self):
        """Testing putting a subset of chunks together."""
        the_vector = np.random.randn(463, 245)
        the_dicts = chunker_nd(the_vector.shape, (11, 7), (6, 3))

        the_chunks = extract_chunks_from_array(the_dicts, the_vector)

        # remove the edge
        def rules(the_array):
            if(0 in the_array.chunk_ndx or
               the_array.chunk_ndx[0] == the_array.chunk_shape[0] - 1 or
               the_array.chunk_ndx[1] == the_array.chunk_shape[1] - 1):
                return False
            return True
        first_dict = the_chunks[0].get_chunk_dict()
        last_dict = the_chunks[-1].get_chunk_dict()

        start_row = first_dict[0]['full_stop'] - first_dict[0]['padding'][1]
        start_col = first_dict[1]['full_stop'] - first_dict[1]['padding'][1]
        stop_row = last_dict[0]['full_start'] + last_dict[0]['padding'][0]
        stop_col = last_dict[1]['full_start'] + last_dict[1]['padding'][0]

        the_chunks = list(filter(rules, the_chunks))

        reassembled_vector = bounding_array_from_chunks(the_chunks)

        np.testing.assert_array_equal(the_vector[start_row:stop_row, start_col:stop_col],
                                      reassembled_vector)

        np.testing.assert_array_equal(reassembled_vector.full_shape, [463, 245])
        np.testing.assert_array_equal(reassembled_vector.full_start, [start_row, start_col])
        np.testing.assert_array_equal(reassembled_vector.full_stop, [stop_row, stop_col])
        np.testing.assert_array_equal(reassembled_vector.padding, np.zeros((2, 2)))
        np.testing.assert_array_equal(reassembled_vector.chunk_ndx, [0, 0])
        np.testing.assert_array_equal(reassembled_vector.chunk_shape, [1, 1])



if __name__ == '__main__':
    unittest.main()
