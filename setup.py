from setuptools import setup

if __name__ == '__main__':
    setup(name="chunkarray", version="0.2", packages=['chunkarray'])
